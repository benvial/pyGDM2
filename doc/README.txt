doc via sphinx

requires: 
 - sphinx
 - Pandoc
 - napoleon (for numpy-docstrings, isntall: `sphinxcontrib.napoleon`)
 - nbsphinx (for jupyter notebook files) 
 
 - cloud-sptheme (website theme)
 
 
 
python module: (md support)
 - recommonmark

 
to install, run:
pip3 install -U sphinx sphinxcontrib.napoleon nbsphinx cloud-sptheme recommonmark
sudo apt install pandoc





Run local http server:

python3 -m http.server 1234
